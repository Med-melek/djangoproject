"""m URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.views.generic import TemplateView

from django.conf.urls import url
from django.contrib import admin
from myapp.views import Hello
from myapp.views import Index
from djgeojson.views import GeoJSONLayerView

from myapp.models import WeatherStation

urlpatterns = [
    url(r'^admin/', admin.site.urls),
   # url(r'^hello/$',Hello,name='hello')
    url(r'^$', Index, name='index'),
    url(r'^', Hello),
    url(r'^data.geojson$', GeoJSONLayerView.as_view(model=WeatherStation), name='data')

]
